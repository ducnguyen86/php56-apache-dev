FROM ubuntu:14.04
MAINTAINER Duc Nguyen <minhduclol@gmail.com>
# Set correct environment variables.
ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive
ENV INITRD No

# Our user in the container
USER root
WORKDIR /var/www/html/

RUN apt-get update -y && apt-get install -y software-properties-common \
  language-pack-en-base \
  git curl wget apache2
RUN a2enmod rewrite
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
#https://lornajane.net/posts/2016/php-7-0-and-5-6-on-ubuntu
RUN yes | add-apt-repository ppa:ondrej/php
RUN apt-get update -y
RUN apt-get install -y php5.6 php5.6-mbstring php5.6-mcrypt php5.6-mysql php5.6-xml php5.6-cgi php5.6-fpm php5.6-common php5.6-curl php5.6-gd php5.6-tidy php5.6-json php5.6-bcmath php5.6-mbstring php5.6-soap php5.6-zip php5.6-bz2 php5.6-dev \
 php-xml php-xhprof php-zip php-pear graphviz
# Set Default PHP Version 5.6
RUN update-alternatives --set php /usr/bin/php5.6
RUN apt-get install -y mysql-client memcached php-memcached
RUN a2enmod proxy_fcgi setenvif
RUN a2enconf php5.6-fpm
#check php version
RUN php -v
RUN pear channel-update pear.php.net
RUN pecl config-set preferred_state beta
RUN pecl install xhprof
#add xhprof vhost
COPY xhprof.conf /etc/apache2/conf-available/xhprof.conf
RUN a2enconf xhprof

RUN yes | pecl install xdebug
RUN echo "zend_extension=$(find /usr/lib/php/ -name xdebug.so)" > /etc/php/5.6/mods-available/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /etc/php/5.6/mods-available/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /etc/php/5.6/mods-available/xdebug.ini

#setup dev value
# 'Set max_execution_time = 300'
RUN find /etc/php/ -type f -name php.ini -exec sed -i 's/max_execution_time = 30/max_execution_time = 300/g' {} ";"
# 'Set memory_limit = 1024M'
RUN find /etc/php/ -type f -name php.ini -exec sed -i -e 's/memory_limit = 128M/memory_limit = 512M/g' {} ";"
# 'Set display_errors = On'
RUN find /etc/php/ -type f -name php.ini -exec sed -i -e 's/display_errors = Off/display_errors = On/g' {} ";"
#install composer
RUN curl -s https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer
#config xhprof http://www.pixelite.co.nz/article/profiling-drupal-7-performance-xhprof-and-devel/
ENV PATH="$HOME/.composer/vendor/bin:$PATH"
RUN composer global require drush/drush:8.*
RUN echo "xhprof.output_dir=\"/tmp/xhprof\"" >> /etc/php/5.6/mods-available/xhprof.ini
RUN mkdir /run/php && chown www-data:www-data /run/php
RUN sed -i '/listen = \/run\/php\/php5.6-fpm.sock/alisten = [::]:9000' /etc/php/5.6/fpm/pool.d/www.conf
RUN echo "DirectoryIndex index.html index.php" >> /etc/apache2/apache2.conf
#RUN service php5.6-fpm restart
RUN apt-get install -y supervisor
COPY supervisor_app.conf /etc/supervisor/conf.d/devwebapp.conf
COPY 000-default.conf /etc/apache2/sites-enabled/000-default.conf
COPY src/ /var/www/html/
# Usage sudo sh /path/to/virtualhost.sh [create | delete] [domain] [optional host_dir]
COPY virtualhost.sh /usr/local/bin/virtualhost
RUN chmod +x /usr/local/bin/virtualhost
# Define mountable directories.
VOLUME ["/var/www/html/", "/tmp/xhprof"]
EXPOSE 80 443
EXPOSE 9000

CMD ["/usr/bin/supervisord"]
#ENTRYPOINT ["/usr/bin/supervisord", "-c /etc/supervisor/supervisord.conf"]
#ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]